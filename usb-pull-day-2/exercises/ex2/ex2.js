function fakeAjax(url,cb) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);

	setTimeout(function(){
		cb(fake_responses[url]);
	},randomDelay);
}

function output(text) {
	console.log(text);
}

// **************************************

function getFile(file) {
	// what do we do here to make a thunk?
	// var args = [].slice.call(arguments, 1);
	// return function(cb){
	// 	args.push(cb);
	// 	file.apply(null, args);
	// }

	var val, fn;

	/**
	 * start by creating and returning a function
	 */
	fakeAjax(file, function(resp){
		// val befor asked
		// val = resp;

		// callback before val
		// fn(resp)

		if(fn){
			fn(resp);
		}else{
			val = resp;
		}
	});

	return function(cb){
		// val befor asked
		// cb(val);

		// callback before val
		// fn = cb

		if(val){
			cb(val);
		}else{
			fn = cb;
		}
	}
}

// request all files at once in "parallel"
// ???

var th1 = getFile("file1");
var th2 = getFile("file2");
var th3 = getFile("file3");

th1(function(text1){
	output(text1);
	th2(function(text2){
		output(text2);
		th3(function(text3){
			output(text3);
			output("complete");
		})
	})
})