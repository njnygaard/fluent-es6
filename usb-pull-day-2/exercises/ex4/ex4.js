function fakeAjax(url,cb) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);

	setTimeout(function(){
		cb(fake_responses[url]);
	},randomDelay);
}

function output(text) {
	console.log(text);
}

// **************************************
// The old-n-busted callback way

function getFile(file) {
	return new Promise(function(resolve){
		fakeAjax(file,resolve);
	});
}

// Request all files at once in
// "parallel" via `getFile(..)`.
//
// Render as each one finishes,
// but only once previous rendering
// is done.

// ???



["file1","file2","file3","file4","file5"]
// one to one mapping of a value to a new value
.map(getFile)
// .reduce(function(acc, val){ // very functional
.reduce(function(chain, promise){
	return chain.then(funciton(){
		return promise;
	})
	.then(output);

}, Promise.resolve()) // returns a resolved promise
.then(function(){
	output('done');
})


/**
 * Abstractions on top of the promise pattern.
 * c: 'gate' === Promise.all()
 * 
 */

/**
 *
 * Anything you pass here will be lifted to a promise. like '42'
 */
Promise.all([
	doTask1(),
	doTask2(),
	doTask3()
])
.then(function(results){
	return doOtherThing(
		Math.max(
			results[0],
			results[1],
			results[2])
		)}
	)