function fakeAjax(url,cb) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);

	setTimeout(function(){
		cb(fake_responses[url]);
	},randomDelay);
}

function output(text) {
	console.log(text);
}

// **************************************
// The old-n-busted callback way

function getFile(file) {

	fakeAjax(file,function(text){
		// what do we do here?
		// output(text);
		organizeResponses(file, text);

	});
}

var files = {};

function organizeResponses(fileName, text){
	// var files = {};

	/**
	 * Some strange syntax that I haven't seen 'in'
	 * @param  {[type]}
	 * @return {[type]}
	 */
	if(!(url in files)){}

	files[fileName] = text;
	if(files.file1 !== undefined &&
	   files.file2 !== undefined &&
	   files.file3 !== undefined ){
		output(files.file1);output(files.file2);output(files.file3);
	}


}

// request all files at once in "parallel"
getFile("file1");
getFile("file2");
getFile("file3");
