'use strict';
/**
 * My try
 */
//var x = 2, fns = [];
//
//
//(function () {
//    var x = 5;
//
//    for (var i = 0; i < x; i++) {
//        fns[i] = function nik() {
//            console.log('i: ', i);
//            return 4;
//        };
//
//    }
//})();
//
//console.log(
//    fns
//);
//
//console.log(
//    (x * 2) === fns[x * 2]()
//);
//
//// true

/**
 * His
 */
var x = 2, fns = [];


{
    let x;
    x = 5;
    /**
     * if you change the for loop to 'let' you get a new i for each
     * iteration! cool.
     */
    for (let i = 0; i < x; i++) {
        //let j = i;
        fns[i] = function () {
            return i;
            /**
             * Every function closes over a different j.
             */
        }
    }
}

console.log(
    fns
);

console.log(
    (x * 2) === fns[x * 2]()
);

// true
