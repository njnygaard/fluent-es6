'use strict';


function foo(...args) {
    let concat = [];
    for (let i in args) {
        console.log(args[i]);
        concat.concat(args[i]);
    }
    console.log('foo, args ', concat)
}

function bar() {
    var a1 = [2, 4];
    var a2 = [6, 8, 10, 12];

    return foo(a1, a2);
}

console.log(
    bar().join("") === "281012"
);
