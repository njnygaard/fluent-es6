function upper(strings, ...values) {
    "use strict";
    for (let thing in values) {
        thing = thing.toUpperCase();
        //values[i] = values[i].toUpperCase();
    }

    let temp;

    for (let i = 0; i < (strings.length + values.length); i++) {
        if (i === strings.length) {
            break;
        }
        temp += strings[i];
        temp += values[i];
    }
    return temp;
}

var name = "kyle",
    twitter = "getify",
    classname = "es6 workshop";

console.log(upper`Hello ${name}, welcome to the fucking ${classname}`);
