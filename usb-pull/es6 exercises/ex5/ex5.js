var numbers = {
};
/**
 * = {} is the most important thing here, because you need to be able to handle
 * line 19 effectively passing undefined at numbers
 * @param start
 * @param end
 * @param step
 */
numbers[Symbol.iterator] = function*({start = 0, end = 100, step = 1} = {}){
    "use strict";
    for(let i = start; i < end; i+=step){
        //console.log(i);
        yield i;
    }
};

// should print 0..100 by 1s
for (let num of numbers) {
    console.log(num);
}

// should print 6..30 by 4s
for (let num of numbers) {
    console.log(num);
}
