/**
 * Block Scoping
 * iffy: immediately invoked function expression.
 * Duplicate declaration is a noop. Only reassigns.
 */

var x = 2;
function bob() {
    var x = 10;
    console.log(x);
}

bob();

/**
 * function that was a declaration now does not encroach on the enclosing scope.
 * Create a scope inside another scope. Protecting the outer scope.
 * Heavyweight.
 * Changes 'this'
 * Changes the meaning of the return statment(?)
 *
 */
(function bob() {
    var x = 10;
    console.log(x);
})();


/**
 * Motivation for block scoping.
 * let keyword is useful for enforcing the choice we wanted all along.
 *
 * When you make them all lets, you lose sight of the fact that lets are for
 * blocks.
 *
 * There is a 'signal' difference between let and var that is useful to
 * developers.
 *
 */
function diff(x, y) {
    //var tmp;
    let z = y / 2;
    if (x > y) {
        let tmp = y;
        y = x;
        x = tmp;
    }
    return y - x;
}

/**
 * TDZ temporal dead zone:
 * Using a variable before it has been assigned.
 * Something from a javascript RFC
 */

/**
 * Implicit scope definitions. Scoped with curly braces for some reason. {}
 */
{
    let spcifically_explicitly_scoped = 'why am i doing this?';
}


/**
 * const ("actively makes things harder to understand")
 * Cannot be reassigned.
 * Some people have suggested that you use const everywhere, and then make
 * the choice that you refactor when you need to reassign. That seems
 * fucking crazy.
 *
 * const -> let -> var
 * Promote when you find that you need the losness of the new feature.
 *
 * His recommendation is that you do the exact opposite.
 *
 * The variable cannot be reassigned, but the value can totally change
 *
 * const x = [2];
 * x[0] = 1; // good
 * x = []; //error
 *
 * const keyword is about signaling the intent about const.
 *
 * Problems:
 * Reassignment is easily scanned for locally.
 * What we really want is a method for object immutability.
 */

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/seal
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze
var frozen = Object.freeze([2, 3, 4]);
/**
 * *SHALLOW* freeze of the array here. cool.
 */


function sum(a, b, c) {
    console.log(a + b + c);
}

sum(3, 4, 5); //12

var nums = [4, 5, 6];
/**
 * very imperative approach
 */
sum.apply(null, nums);

sum(...nums);
/**
 * There are other 'iterators' other than arrays.
 * Arrays are iterable, but not the only iterators.
 * ... in ES6 can be used with iterators.
 */

var arr1 = [1, 2, 3];
var arr2 = [4, 5, 6];

var arr3 = [0].concat(arr1, arr2, [7, 8, 9]);

/**
 * ES6 declarative form. Spread operator.
 */
var arr3 = [0, ...arr1, ...arr2, 7, 8, 9];


/**
 * ... operator continued.
 * Him: gather and spread operators
 * Everyone else calls 'gather' rest. cool.
 */

function foo(x, y, ...rest) {
    //var rest = [].slice.call(arguments);

    console.log(x, y, rest);
}
foo(1, 2, 3, 4, 5);


/**
 * Defaults
 * x = x || 10;
 * Default parameter idiom.
 *
 * x = (x !== undefined) ? x : 10;
 * very imperative.
 */

function defaults(x = 10, y = 42) {
    console.log(x, y);
}

foo(...[]); // 10 42


/**
 * Default value expressions are lazy expressions.
 * They are not executed until they are needed.
 * These are not 'eagerly' evaluated.
 *
 * Fuck this noise. This topic is stupid. dont use.
 */

function bar() {
    return 10;
}

function baz() {
    return 42;
}

function foo(x = bar(), y = baz()) {
    console.log(x, y);
}


/**
 * Doozie
 * Destructuring
 * Pattern for our assignments when the pattern is in the l-value area.
 * Array notation in an l-value position has special meaning.
 */
var [a,b,c] = [1, 2, 3];

function foo() {
    return [1, 2, 3];
}

var [x,y,...rest] = foo();

console.log(x, y, rest); // 1,2,[3]

/**
 * This is not just sugar
 * This is the most important example of declarative coding in javascript. cool.
 */

function foo() {
    return {x: 1, y: 2, z: 3}
}

var {x:x,y:y,z:z} = foo();
/**
 * also concise properties
 * if the property is the same name, you don't need the stuff.
 * @type {{x, y, z}|*}
 */
var {x,y,z} = foo();


//x = {a:1, b:2, c:3};
// prop:value
//
//{
//    a: varA,
//    b: varB,
//    c: varC,
//} = x;
//
//property: variable;

/**
 * Destructuring is most appropriate to be thought of as an assignmnet
 * pattern, not a declaration pattern.
 */
var o = {};
function foo() {
    return [1, 2]
}

// You need to wrap this in parenthesis if there is no declarator like 'var'
([o.x, o.y, o.z] = foo());


/**
 * You can do this with objects too. He went a little too fast on this
 * topic. Revisit this thing.
 */

/**
 * If there was a null returned, you can't set a default value for a
 * property on null...
 * @returns {null}
 */
function foo() {
    return null;
}

var {
    a: x,
    b: y = 12,
    c: z
    } = foo() || {};


/**
 * This got really complicated, just revisit it later.
 */
function foo({
    x: y
    }) {
    console.log(y); //2
}

foo({x: 2});


function foo([
    x = 10,
    y = 12,
    z = 14
    ]) {
    console.log(y); //2
}

foo([1, 2, 3]);


/**
 * Nested destructuring
 */

// array
function foo() {
    return [[1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]];
}

var [
    [,,x],
    [],
    [y,z,]
    ] = foo();
console.log(x, y, z); // 3 7 8


/**
 * Okay that's cool, JSON declarative structure
 * @returns {{x: number[], y: *[], z: number}}
 */
//object
function foo() {
    return {
        x: [1, 2, 3],
        y: [{
            z: [4, 5]
        }],
        z: 6
    };
}

var {
    x: [X1, X2],
    y: [
        {
            z:[Z1, Z2]
            }
        ],
    z: Z3
    } = foo();
console.log(X1, X2, Z1, Z2, Z3); // 1, 2, 4, 5, 6

/**
 * Defaults too
 */
//object
function foo() {
    return {
        x: [1, 2, 3],
        y: [{
            z: [4, 5]
        }],
        z: 6
    };
}

var {
    x: [X1, X2] = [],
    y: [
        {
            z:[Z1 = 3, Z2] = []
            }
        ] = [],
    z: Z3
    } = foo() || {};
console.log(X1, X2, Z1, Z2, Z3); // 1, 2, 4, 5, 6


/**
 * He made this up.
 * Destructuring and restructuring
 * This is hte next example.
 *
 */

var defaults = {
    foo: 2,
    bar: {
        baz: 6,
        bam: 'hello'
    }
};

var config = {
    bar: {
        bam: 'HELLO'
    }
};

{
    let {
        foo = defaults.foo,
        bar: {
            baz = defaults.bar.baz,
            baz = defaults.bar.bam
            }
        }

    config = {
        foo,
        bar: {
            baz,
            bam
        }
    }
}


/**
 * Concise methods
 */
var x = 2;
// ES5
obj = {
    x: x,
    y: function () {
    }
};
obj[z] = 42
// ES6
obj = {
    x,
    y(){
    },
    [z]: 42, // z can be an expression?
    [x](){
        "use strict";
        //...
    },
    get bar() {
        return this.__bar;
    },
    set bar(x) {
        this.__bar = x
    }
};

obj["2"](); // valid. "2" was coerced in to a string.


/**
 * He's just going over random stuff...
 */

var o1 = {
    foo: 10
};

var o2 = Object.create(o1);

console.log(o2.foo); // 10

//ES6
var o2 = {
    __proto__: o1 // Must be set at declaration. "dunderproto". This pattern
    // seems to be frowned upon. http://www.2ality.com/2012/10/dunder.html
};

Object.setPrototypeOf(o2, o1);

// preferred
var o2 = Object.assign(Object.create(o1), {
    //..
});


/**
 * Template literal
 * He calls it: Interpolated String Literal
 *
 *
 */
//ES5
var name = "kyle", title = "teacher";

var msg = "hello" + name + title;

//ES6
var msg = `Hello , ${name} you are the ${title}.`;
/**
 * Anything inside the braces can be valid javascript.
 */


/**
 * Multi line strings.
 */

var x = "Hello, this is a really long sentence\
more words\
multi line";

//ES6 valid
var msg = `Hello , ${name}
 you are
 the ${title}.`;


/**
 * Tag template literals
 * Tag interpolated string literals.
 * It is invariant that you get one more value in strings array than values
 * array.
 */
var msg = foo`Hello , ${name} you are the ${title}.`; // other string
// Strings is all the literal stuff in that string.
function foo(strings, ...values) {
    "use strict";

    console.log(strings.raw);// strings with newline characters in them.
    return 'other string'
}


/**
 * Not a general purpose feature.
 * Enabler of other features.
 *
 * New datatype: Symbol
 *
 * NOT newable
 * simple primative
 *
 * Globally unique to your program, unguessable.
 * You cannot see the value.
 * You can use it but never see it.
 *
 * 'metaprogramming'
 * Control and extend the behavior of the language.
 *
 * 99.99% you will use a symbol as a property of an object.
 *
 * useful as globally unique for your program.
 * I think this is very useful.
 */

var a = Symbol("some description for the value");
a; // Symbol(some description from value)

var obj = {};

var a = Symbol();

obj[a] = 42;

console.log(obj[a]); //42

Object.getOwnPropertySymbols(obj);
//Object.getOwnPropertyNames();
//Object.getOwnPropertyDescriptor();


/**
 * Well-known symbols
 */
var obj = {};

console.log(obj[Symbol.toStringTag]); // [object Object]
obj[Symbol.toStringTag] = function () {
}; // You can do your own
// thing when someone stringifys your object


/**
 * Symbol.iterator
 *
 */

var a = [1, 2, 3];
var b = "abc";

var it_a = a[Symbol.iterator]();
var it_b = b[Symbol.iterator]();

it_a.next(); // {value: 1, done: false}
it_a.next(); // {value: 2, done: false}
it_a.next(); // {value: 3, done: false}
it_a.next(); // {value: undefined, done: true}


/**
 * NEW LOOP
 * for-of
 */

for (let i = 0; i < a.length; i++) {
    console.log(a[i]);
}

for (var i in a) {
    console.log(a[i]);
}

/**
 * Checks the iterator symbol on the object
 * This loop is the reason the iterator exhausts all the values before it
 * says done:true
 */
for (var v of a) {
    console.log(v);
}


/**
 * what is an iterator
 */

b[Symbol.iterator] = function () {
    "use strict";
    var context = this, idx = 0;
    return {
        next(){
            if (idx < context.length) {
                return {
                    value: context[idx++],
                    done: false
                }
            }
            else {
                return {done: true};
            }
        }
    }
};

/**
 * Objects do not by default expose an iterator.
 * look at ashley's notes
 */

/**
 * Generators
 *
 * when invoked, wont run.
 * instead produces a function
 */
function *generated() {
    //"use strict";
    console.log(3);
    console.log(3);
    console.log(3);
    yield;
    console.log(4);
    console.log(4);
    console.log(4);
}

var it = generated();

it.next();// 3 3 3
it.next();// 4 4 4


/**
 * new things
 */

function *generated() {
    //"use strict";

    yield 1;
    yield 2;
    yield 3;
    return 4;
}

var cool = generated();

cool.next();// {value:1, done:false}
cool.next();// {value:2, done:false}
cool.next();// {value:3, done:false}
cool.next();// {value:4, done:true}
cool.next();// {value:undefined, done:true}

for (var v of generated()) {
    console.log(v);
}

/**
 * Generators automatically make generators.
 */

var b = "abc";

b[Symbol.iterator] = function*() {
    "use strict";
    var idx = 0;
    while (idx < this.length) {
        yield this[idx++];
    }
};


/**
 * Map
 * the purpose of map is to have keys that are other than strings
 */
var b = new Map();

var k1 = [1, 2, 3];
var k2 = [4, 5, 6];
var v1 = "hello";
var v2 = "world";

b.set(k1, v1);
b.set(k2, v2);

b.keys();// iterator over keys

for (var key of b.keys()) {
    console.log(key);
}


/**
 * map: entries
 */
for (var e of b.entries()) {
    console.log(key);
}
// [[1,2,3],"hello"]
// [[4,5,6],"world"]

for (var [key, value] of b.entries()) {
    console.log(key, value);
}
// [1,2,3] "hello"
// [4,5,6] "world"


/**
 * Arrow Functions
 * Functional programming things.
 * By definition anonymous when used as callbacks
 *
 * Arrow functions do not have a this binding.
 * Performes a lexical lookup to the closest this
 */

/**
 * I think I'm just going to have to deal with long comment lines in sublime
 * text. 
 * You can easily setup a ruler for 80 characters and manage the problem 
 * yourself.
 */

/**
 * Day 2
 * Kyle Simpson
 * @getify
 * http://getify.me
 *
 * Personal projects:
 * LABjs
 * grips
 * asynquence
 *
 * Book Signing right after 10:20 keynote, like 11:00 am
 *
 */

/**
 * Parallelism vs Asynchronisity
 * Not equivalent, symptoms of larger concurrency topic.
 */

/**
 * New baseline to call yourself a JavaScript developer.

• Parallel vs Async
• Callbacks
• Thunks
• Promises
• Generators/Coroutines
 */

/**
 * CSP: Communicating Sequential Processes
 * He's fascinated by this pattern in his own work.
 */

/**
 * Parallelism
 *
 * Everything is single threaded in javascript.
 * Never had to use mutexes or semaphores.
 *
 * Servo, Mozilla making an attempt at off-thread garbage collection.
 *
 * 
 */

/**
 * Concurrency
 * Two or more tasks occuring in the same time frame.
 * Macro: ajax
 * Micro: all the things to make the ajax call work.
 *
 * 
 */

/**
 * Timers:
 * http://blog.getify.com/on-the-nature-of-timers/
 *
 * Callback: Marked off a certain part of the code as the reentry ponit. GOTO
 * Stuff that happens now, synchronously. And stuff that happens later.
 *
 * Callback hell: Expression of a temporal dependency without other tools 
 * given to us.
 *
 * Callback hell has nothing to do with indentation or nesting.
 * The problem with nesting and indentations is a visibility problem.
 * 
 */




/**
 * Callback hell (cont)
 *
 * Inverison of control
 * as a concept, not necessarily bad.
 * As related to callbacks, really bad.
 *
 * "Inversion of control is what turns something from a library in to a framework."
 * Page 21:
 *  Line 1&2, are in your control.
 *  Line 3&4 are not in your control anymore.
 *
 * We have a choice:
 *  Code such that we can't trust the system.
 *  Search for a better solution.
 * 
 */

/**
 * Thunk
 *
 * A thunk is a function that has everything it needs to do stuff.
 * Time is sthe most ocomplex state in your application.
 * Looking for patterns that manage time for us.
 * Thunks abstract time for us.
 *
 * An eager Thunk starts executing right now. Go get all the stuff you need.
 *
 * Lazy Async Thunk, no race condition.
 * Eager, we might get the answer back before someone asks for it.
 *
 *
 * Manage state over time by using Closure.
 *
 * 
 */

/**
 * Promises
 * Once a promise is resolved, it is immutable.
 * Callback managment system.
 * 
 */
// "Lifting a function"
function foo(x,y,cb){}

function fooPromised(x,y){
    return new Promise(function(resolve, reject){
       foo(x,y, function(err,answer){
            if(err) reject(err);
            else resolve(answer);
        }) 
    })
}




/**
 * Flow control with promises
 * "I'm so beyond this that I don't do it anymore" lol
 * code hipster
 *
 * Chaining Promises
 * fluent style apis thing().other().keepGoing()
 *
 * page 56
 * doSecondThing() returns a promise and it is 'merged' with the promise that .then generates (under the hood)
 * 
 * In the success callback from promise 1, return a promise.
 *
 * 
 */



/**
 * Generators
 * pause at the yied keyword.
 * yield is an outbound message passing system.
 * yield is two way
 *
 * Generators wait for yeild.
 * send stuff in and get stuff out several times.
 *
 * It is okay to only run part of the generator.
 *
 *
 * Pattern for writing synchronous-looking asynchronous code.
 * page 76 lines 2-4 are synchronous-looking asynchronous code that makes time and implementation detail.
 *
 * 
 */



/**
 * generator + promises
 * More on this topic.
 * https://davidwalsh.name/es6-generators
 */

/**
 * Observables
 * What is an observable?
 * https://github.com/Reactive-Extensions/RxJS
 *
 * http://rxmarbles.com/#distinctUntilChanged
 * visualizing a very complex topic
 *
 * https://github.com/getify/a-tale-of-three-lists
 */




/**
 *
 */
var lastCode = 0